
from numpy import *
from labtools import *

analytic = lambda x: e**(-10*x)

def bashforth():
    h = 1
    y = zeros(6)
    y[0] = 1
    y[1] = 4e-5
    for i in range(2, 6):
        y[i] = y[i-1] + h/2 * (3 * (-10) * y[i-1] - (-10 * y[i-2]))
    return y

def adams_moulton():
    h = 1
    y = zeros(6)
    y[0] = 1
    y[1] = 4e-5
    for i in range(2, 6):
        y[i] = 1/(1+10/3*h) * (y[i-1] + h/3 * (2*y[i-2] - 16*y[i-1]))
    return y


x = array([0, 1, 2, 3, 4, 5])
xs = linspace(0, 5, 10000)
plt.plot(xs, analytic(xs),'b-', label = 'analytische Lösung')
#plt.plot(x, bashforth(),'rx',  label = 'Bashforth')
plt.plot(x, adams_moulton(),'gx', label = 'Moulton')
plt.legend(loc = 'lower left')
plt.xlabel('$t$')
plt.ylabel('$u(t)$')
plt.savefig('num.pdf')

t = lt.SymbolColumnTable()
t.add_si_column(r"t", x, places=0)
t.add_si_column(r"y_\t{Bash.}", bashforth(), places=4)
t.add_si_column(r"y_\t{Moult.}", adams_moulton(), places=4)
t.savetable("table.tex")

