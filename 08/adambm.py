from __future__ import division
from pylab import *

y0=1
y1=4e-5

f=lambda u: -10*u
g=lambda x: exp(-10*x)

x=linspace(0,5,1000)

h=1
bashy =array([y0,y1,0,0,0,0])
moulty =array([y0,y1,0,0,0,0])
t=array([h*k for k in range(len(bashy))])

for i in range(2,len(bashy)):
    bashy[i]=bashy[i-1]+h/2*(3*f(bashy[i-1])-f(bashy[i-2]))
    moulty[i]=1/(1-h*50/12)*moulty[i-1]+h/(12*1+h*50/12)*(8*f(moulty[i-1])-f(moulty[i-2]))

print bashy, moulty, g(t)
rc('text', usetex=True)
fig=figure()

plot(t, bashy, 'rx', label = 'Bashforth')
plot(t, moulty, 'bx', label='Moulton')
plot(x, g(x), 'g', label=r'$\mathrm{e}^{-10 \cdot t}$')
legend(loc = 'lower left')

xlabel('$t$')
ylabel('$u(t)$')
fig.tight_layout()
savefig('adam.pdf')