from __future__ import division
from pylab import *

f = lambda t: sin(t)
df = lambda t: cos(t)
x = 0.3

hs = [ 0.4*0.5**k for k in range(0, 50) ]

result = [[], [], []]
errors = [[], [], []]

for h in hs:
    result[0].append( (f(x + h) - f(x)) / h )
    result[1].append( (f(x) - f(x - h)) / h )
    result[2].append( (f(x + h) - f(x - h)) / (2 * h) )

for i in [0, 1, 2]:
    errors[i] = abs(result[i] - df(x))

fig = figure()

ks = list(range(1, 51))

labels = ['forward', 'backward', 'central']

for i in [0, 1, 2]:
    ax = fig.add_subplot(3, 1, i+1)
    ax.plot(ks, errors[i], 'x', label = labels[i])
    ax.legend(loc = 'lower right')
    ax.set_yscale('log')

xlabel('$k$')
fig.tight_layout()

savefig('errors.pdf')
