
from __future__ import division, print_function
from numpy import *

from labtools import *

def round_figures(x, n):
    return round(x, n - int(floor(log10(abs(x)))) - 1)

f = lambda t: sin(t)
x0 = 0.3

result_analytic = cos(x0)
result_numeric = []
result_errors = []

# for convenience
r = lambda x: round_figures(x, 4)

hs = arange(0.03, 0.006, -0.002)
for h in hs:
    deriv = r(( r(f(r(x0) + r(h))) - r(f(r(x0))) ) / r(h))
    result_numeric.append(deriv)
    result_errors.append(abs(deriv - result_analytic))

print(result_errors)

t = lt.SymbolColumnTable()
t.add_si_column('h', hs)
t.add_si_column('\err{\d{t}{f};}', result_errors, places=4)
t.savetable('table1.tex')
