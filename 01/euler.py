#!/usr/bin/env python3

'''
Simple numeric differential equation approximator
Igor Babuschkin 2012
'''

from labtools import *

def euler(f, init, interval, h, maxsteps=None):
    '''
    Approximates a solution to the first-order
    differential equation 
        u' = f(t, u(t))
    using Euler's method.

    Parameters
    ----------
    f : function
        The function on the right-hand side in u' = f(t, u(t)).
    init : array_like
        The initial conditions for u(t). Must have the same
        type and dimensions as the return type of f.
    interval : tuple
        The interval in which values should be returned.
    h : float
        The step size to be used.
    maxsteps: int
        Number of steps after which the calculation should
        be aborted.
    '''
    n = int((interval[1] - interval[0]) / h)
    if maxsteps:
        n = min(maxsteps, n)
    shape = (n, len(init))
    u = np.zeros(shape)
    u[0] = init

    for i in range(n-1):
        u[i+1] = u[i] + h * f(h * i, u[i])

    return u

mpl.rc('figure', figsize='5.3964, 6')

A = np.array([[ -21,  19, -20],
           [  19, -21,  20],
           [  40, -40, -40]])

f = lambda t, u: np.dot(A, u)

u_analytic = [
    lambda t: 0.5 * np.exp(-2 * t) + 0.5 * np.exp(-40 * t) * (np.cos(40 * t) + np.sin(40 * t)),
    lambda t: 0.5 * np.exp(-2 * t) - 0.5 * np.exp(-40 * t) * (np.cos(40 * t) + np.sin(40 * t)),
    lambda t: -1 * np.exp(-40 * t) * (np.cos(40 * t) - np.sin(40 * t))
]

interv = (0, 0.4)
u_0 = [1, 0, -1]

# draw analytic solutions
for i, func in enumerate(u_analytic):
    plt.subplot(3, 1, i+1)
    x_smooth = np.linspace(interv[0], interv[1], 10000)
    plt.xlim(*interv)
    plt.plot(x_smooth, func(x_smooth), label='analytic')

# draw approximations
for h in [0.02, 0.01]:
    u = euler(f, u_0, interv, h)
    x = np.arange(interv[0], interv[1], h)
    for i, u_i in enumerate(u.T):
        plt.subplot(3, 1, i+1)
        plt.plot(x, u_i, label='numeric, $h = {}$'.format(h))
        plt.legend()

plt.tight_layout()
plt.savefig(lt.new('euler.tex'), bbox_inches='tight', pad_inches=0)
