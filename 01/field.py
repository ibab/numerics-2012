#!/usr/bin/env python3

from labtools import *

mpl.rc('text', usetex=True)
mpl.rc('lines', markeredgewidth=1.4)
mpl.rc('lines', markersize=12)

x = np.linspace(-1.5, 1.5, 30)
y = np.linspace(-1.5, 1.5, 30)
X, Y = np.meshgrid(x, y)
deg = np.arctan(-X/Y)
QP = plt.quiver(X, Y, np.cos(deg), np.sin(deg))

h = 0.3
xs = np.arange(0, 1, h)
us = np.zeros(len(xs))
us[0] = 1

for i in range(len(xs)-1):
    us[i+1] = us[i] - h * (xs[i]) / us[i]

plt.plot(xs, us, 'r-x')

t = lt.SymbolColumnTable()
t.add(lt.SymbolColumn("t", xs, places=1))
t.add(lt.SymbolColumn("u(t)", us))
t.savetable(lt.new('field-tab.tex'))

plt.tight_layout()
plt.savefig(lt.new('field.pdf'), bbox_inches='tight', pad_inches=0)
