from __future__ import division
from numpy import *
from pylab import *
x0=5
step=6
f = lambda x: cos(x)-x
g = lambda x: -sin(x)-1
x=zeros(step)
y=array([x0,0,0,0,0,0])
for i in range(len(x)-1):
	x[i+1]=x[i]-f(x[i])/g(x[i])
	y[i+1]=y[i]-f(y[i])/g(y[i])

print (x ,y)

