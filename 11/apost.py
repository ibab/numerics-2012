from __future__ import division 
from numpy import *

x_0=0
def apost(x,x_last):
	return sin(1)/(1-sin(1))*abs(x-x_last)

def fixpunkt(x_0):
	x=x_0
	x_last=x_0
	while True:
		x=cos(x)
		yield x, apost(x,x_last)
		x_last=x

for n,(x,err) in enumerate(fixpunkt(0)):
	print(x, err, n + 1)
	if(err <= 1e-5):
		break
