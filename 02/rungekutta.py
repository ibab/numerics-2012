from labtools.table import *
from numpy import *

def runge_kutta(f, init, interval, h, a, b, c):
    '''
    Generic Runge-Kutta implementation for solving ODEs.
    The coefficients have to be supplied.
    Can be used for approximating both scalar and vectorial functions.

    parameters
    ==========
    f:          Right-hand side function in u' = f(t, u(t))
    init:       Initial value u(interval[0])
    interval:   Interval for which the solution should be calculated
    h:          Step size in approximation
    a, b, c:    Runge Kutta coefficients
    '''

    # convert scalar input to 1-d vector
    if not isinstance(init, ndarray):
        init = array([init])

    n = int((interval[1] - interval[0]) / h)

    shape = (n, len(init))
    u = zeros(shape)
    u[0] = init
    k = zeros(len(c))

    for i in range(n-1):
        t = interval[0] + i * h
        k[0] = f(t, u[i])
        for j in range(1, len(k)):
            k[j] = f(t + a[j] * h, u[i] + h * sum(b[j][:j-1] * k[:j-1]))
        u[i+1] = u[i] + h * sum(c*k)

    # recover scalar input from 1-d vector
    if len(u[0]) == 1:
        u = u[:,0]

    return u

if __name__ == '__main__':
    # ODE to be solved
    f = lambda t, u: 5 * u - u**2

    # analytic solution for comparison
    u_analytic = lambda t: 1/(1/5 + 4/5 * e**(-5*t))

    # initial value
    init = 1

    # solution interval
    interval = (0, 5)

    # Runge-Kutta 3/8 coefficients
    a = array([None, 1/3, 2/3, 1])
    b = array([None,
               [ 1/3 ],
               [-1/3,   1 ],
               [   1,  -1,   1 ]])
    c = array([1/8, 3/8, 3/8, 1/8])

    hs = [ 0.5**k for k in range(1, 12) ]
    norms = []

    for h in hs:
        u = runge_kutta(f, init, interval, h, a, b, c)
        t = linspace(interval[0], interval[1], len(u))
        norm = max([ abs(a-b) for a, b in zip(u, u_analytic(t))])
        norms.append(norm)

    t = SymbolColumnTable()
    t.add_si_column('h', hs, places=4)
    t.add_si_column(r'\max(\abs{u-u_\t{analytic}})', norms, places=10)
    t.savetable('result.tex')

