/*
 * Quick and dirty implementation of the Runge-Kutta 3/8 method
 *
 */

#include <stdlib.h>
#include <stdio.h>

#define f(t, u) (5 * (u) - (u)*(u))
#define min(a, b) (a < b? a: b)

double* runge_kutta(double *y, int n, double y_0, double left, double right, double h) {
    /*
     * parameters
     * ==========
     * y:     Pre-initialized result array.
     * n:     Length of y.
     * y_0:   Initial value, y_0 = y(left)
     * left:  left border of interval
     * right: right border of interval
     * h:     step size
     */
    double k1, k2, k3, k4;
    double t;
    int i;

    n = min(n, (int)((right - left) / h));
    y[0] = y_0;

    for(i=0; i<n-1; i++) {
        t = left + i * h;
        k1 = f(t, y[i]);
        k2 = f(t + 1.0/3 * h, y[i] + 1.0/3 * h * k1);
        k3 = f(t + 2.0/3 * h, y[i] - 1.0/3 * h * k1 + h * k2);
        k4 = f(t + h, y[i] + h * k1 - h * k2 + h * k3);
        y[i+1] = y[i] + 1.0/8 * h * (k1 + 3*k2 + 3*k3 + k4);
    }
    return y;
}

void save_result(double *y, int n, double h, char *path) {
    int i;
    FILE *f = fopen(path, "w");

    if ( f == NULL )
    {
        fputs("Error opening file file\n", stderr);
        fclose(f);
        exit(EXIT_FAILURE);
    }

    for(i=0; i<n; i++) {
        fprintf(f, "%.16f %.16f\n", i*h, y[i]);
    }
    fclose(f);
}

int main() {
    double h = 0.01;
    double left = 0;
    double right = 5;
    double init = 1;
    int n = (int) ((right - left) / h);
    double *y = malloc(n * sizeof(double));

    runge_kutta(y, n, init, left, right, h);

    save_result(y, n, h, "result");

    free(y);

    return 0;
}
