
import numpy
from matplotlib import pyplot as pt

# analytic result
u = lambda t: 1/(1/5 + 4/5 * numpy.exp(-5*t))
ts = numpy.linspace(0, 5, 10000)

t, y = numpy.loadtxt('result', unpack=True)

pt.plot(t, y)
pt.plot(ts, u(ts), 'r--')

pt.savefig('result.pdf')
